
ifeq ($(NAME),)
$(error NAME must be defined)
endif

include debian/config/$(NAME).mk

build: $(GEN_FILES)

clean:
	$(RM) $(GEN_FILES)
