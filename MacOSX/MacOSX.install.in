#!/bin/bash
# @WARNING@

set -e

usage() {
	echo "Usage: $(basename "$0") [--force] [user]"
	echo "  --force: forcer les opérations (à éviter sans conseil personnalisé)"
	echo "  user: nom du compte utilisateur utilisé pour la sauvegarde"
	echo "        (défaut : '_datapol-backuppc', à garder sans conseil personnalisé)"
}

CUR_LOG_LEVEL=2

LOG_LEVEL_ERR=5
LOG_LEVEL_WARN=4
LOG_LEVEL_NOTICE=3
LOG_LEVEL_INFO=2
LOG_LEVEL_DEBUG=1

log() {
	local prefix=
	local level=$LOG_LEVEL_INFO

	while : ; do
		case "$1" in
		--prefix) prefix="$2"; shift ;;
		--level) level="$2"; shift ;;
		--) shift ; break ;;
		--*) error "Internal error" "$1 option not supported for loging" ;;
		*) break;;
		esac
		shift
	done

	if [ $level -lt $CUR_LOG_LEVEL ]; then
		return 0
	fi

	for m in "$@"; do
		echo 1>&2 "$prefix$m"
	done
}

error() {
	log --level $LOG_LEVEL_ERR --prefix "E: " "$@" "Aborting."
	exit 1
}

warning() {
	log --level $LOG_LEVEL_WARN --prefix "W: " "$@"
}

notice() {
	log --level $LOG_LEVEL_NOTICE --prefix "N: " "$@"
}

info() {
	log --level $LOG_LEVEL_INFO --prefix "I: " "$@"	
}

debug() {
	log --level $LOG_LEVEL_DEBUG --prefix "D: " "$@"	
}

if ! uname -s | grep -sqi darwin ; then
    error "Ce script est prévu pour fonctionner sur MacOSX"
fi

FORCE=
if [ "$1" = "--force" ]; then
	warning "--force enabled"
	FORCE=1
	shift
fi
buser="${1:-_datapol-backuppc}"
info "Local user will be $buser"

if [ "$(id -u)" != 0 ]; then
	error "Ce script doit être lancé en tant que root (avec 'sudo' par exemple)"
fi

if [ -z "$buser" ]; then
	error "Empty local user"
fi

create_user() {
    local USERNAME="$1"
    local FULLNAME="$2"
    local SECONDARY_GROUPS="$3"
    local HOMEDIR="${4:-/Users/$USERNAME}"
    local GROUPID
    local MAXID
    local USERID

    set -e

    info "Creating '$USERNAME' user"

    # Create the group
    dseditgroup -o create -c "Primary group of the _datapol-backup user" $USERNAME
    GROUPID=$(dscl . -read /Groups/$USERNAME | awk '($1 == "PrimaryGroupID:") { print $2 }')
    
    # Find out the next available user ID
    MAXID=$(dscl . -list /Users UniqueID | awk '{print $2}' | sort -ug | tail -1)
    USERID=$((MAXID+1))

    # Create the user account
    dscl -q . -create /Users/$USERNAME
    dscl -q . -create /Users/$USERNAME UserShell /bin/bash
    dscl -q . -create /Users/$USERNAME RealName "$FULLNAME"
    dscl -q . -create /Users/$USERNAME UniqueID "$USERID"
    dscl -q . -create /Users/$USERNAME PrimaryGroupID "$GROUPID"
    dscl -q . -create /Users/$USERNAME NFSHomeDirectory "$HOMEDIR"

    #dscl -q . -create /Users/$USERNAME passwd "*"
    
    # Add use to any specified groups
    for GROUP in $SECONDARY_GROUPS ; do
	dseditgroup -o edit -t user -a $USERNAME $GROUP
    done
    
    # Create the home directory
    mkdir -p "$HOMEDIR"
    chown $USERNAME:$USERNAME $HOMEDIR

    notice "Created user #$USERID: $USERNAME ($FULLNAME)"
}

if ! dscl -q . -list /Users/$buser >/dev/null 2>&1 ; then
    debug "No '$buser' user, need to create it"
    create_user $buser "Backuppc user for datapol server" "" /var/lib/datapol-backuppc/home
fi

bhome="$(eval echo ~$buser)"

if [ -z "$bhome" -o "$bhome" = '~'"$buser" ]; then
	error "L'utilisateur '$buser' n'a pas de HOME." \
		"Avez vous vraiment créé cet utilisateur ?"
fi
if [ ! -d "$bhome" ]; then
	error "Le répertoire '$bhome' n'existe pas." \
		"Avez vous correctement créé l'utilisateur '$buser' ?"
fi
bgroup="$(id -g -n "$buser")"

info "Creation du script /usr/local/share/datapol-backuppc/run-backuppc-rsync..."
mkdir -p /usr/local/share/datapol-backuppc
cat > /usr/local/share/datapol-backuppc/run-backuppc-rsync <<EOF
@run-backuppc-rsync@
EOF
chmod +x /usr/local/share/datapol-backuppc/run-backuppc-rsync
info "done."

info "Création des fichiers de config SSH dans $bhome..."
mkdir -p "$bhome"/.ssh
cat > "$bhome"/.ssh/authorized_keys <<EOF
@authorized_keys@
EOF

chown -Rv $buser:$bgroup "$bhome"/.ssh
info "done."

info "Création d'une entrée sudo pour $buser"
while true; do
	if grep -sq "^$buser " /etc/sudoers ; then
		if [ -z "$FORCE" ]; then
			warning "  $buser already in /etc/sudoers." \
				"  Use --force to update this file."
			break
		fi
		notice "$buser already in /etc/sudoers" \
			"but --force provided, so updating anyway"
		grep -v "^(# datapol-backuppc:|$buser) " < /etc/sudoers \
			> /etc/sudoers.backuppc
	else
		cp /etc/sudoers /etc/sudoers.backuppc
	fi
	echo >> /etc/sudoers.backuppc
	cat >> /etc/sudoers.backuppc <<EOF
@sudoers@
EOF
	# not "mv" to keep permissions and other meta-data
	cat /etc/sudoers.backuppc > /etc/sudoers
	rm /etc/sudoers.backuppc
	break
done
info "done."

info "Autorisation d'accès SSH (remote) pour $buser"
SSH_STARTED=no
if systemsetup -getremotelogin | grep -i off; then
    notice "  Enabling and starting SSH server"
    if ! systemsetup -f -setremotelogin on; then
	notice "    Trying an alternative method to start SSH"
	launchctl load -w /System/Library/LaunchDaemons/ssh.plist
	# To disable:
	# launchctl unload /System/Library/LaunchDaemons/ssh.plist
    fi
    if systemsetup -getremotelogin | grep -i off; then
	error "  Cannot start SSH server."
    fi
    SSH_STARTED=yes
fi
SSH_GROUP=com.apple.access_ssh
if ! dscl -q . -list /Groups/com.apple.access_ssh 2>/dev/null ; then
    if ! dscl -q . -list /Groups/com.apple.access_ssh-disabled 2>/dev/null ; then
	error "   No SSH system group!"
    fi
    if [ $SSH_STARTED = yes ]; then
	notice "   Restricting SSH to allowed users"
	dscl . change /Groups/com.apple.access_ssh-disabled RecordName com.apple.access_ssh-disabled com.apple.access_ssh
    else
	SSH_GROUP=com.apple.access_ssh-disabled
	warning "   not changing current setup that allows ALL users to remote login"
    fi
fi
notice "   Adding $buser to allowed SSH users group"
dseditgroup -o edit -a $buser -t user $SSH_GROUP
info "done."

buser=backuppc
while dscl -q . -list /Users/$buser >/dev/null 2>&1 ; do
    notice "Old 'backuppc' user existing. Removing it..."

    bhome="$(eval echo ~$buser)"
    dscl -q . -delete /Users/$buser
    if [ -d "$bhome" ]; then
	notice "  Removing its HOMEDIR"
	tar -c -f - "$bhome" | gzip -9 > /tmp/$buser.tar.gz
	rm -rf "$bhome"
        notice "  Done (files available in /tmp/$buser.tar.gz)"
    fi

    break
done
if [ -d "/usr/local/share/memo-backuppc" ]; then
    notice "Removing /usr/local/share/memo-backuppc"
    rm -v /usr/local/share/memo-backuppc/run-backuppc-rsync || true
    rmdir /usr/local/share/memo-backuppc || true
fi
if grep -sq "^$buser " /etc/sudoers ; then
    info "Removing old entry in /etc/sudoers"
    grep -v "^(# backuppc:|$buser) " < /etc/sudoers \
	 > /etc/sudoers.backuppc
    # not "mv" to keep permissions and other meta-data
    cat /etc/sudoers.backuppc > /etc/sudoers
    rm /etc/sudoers.backuppc
fi

cat <<EOF



               L'installation semble s'être bien passée.

Si ce n'est pas déjà fait :
* Contacter les CMI (de préférence Vincent Danjean) pour reconfigurer backuppc
  et ajouter votre machine à la liste des machines sauvegardées.
* Pour exclure si nécessaire des fichiers ou des dossiers de la sauvegarde,
  lire la page web suivante :

https://gforge.inria.fr/plugins/mediawiki/wiki/datapol/index.php/Sauvegardes#Choix_des_fichiers_sauvegard.C3.A9s

* Pour voir l'état de vos backups, allez (avec vos identifiants LDAP LIG) sur
  la page suivante :

https://lig-backuppc2.imag.fr/backuppc


EOF

if /usr/libexec/ApplicationFirewall/socketfilterfw --getstealthmode | grep -iq enable; then
    cat <<EOF
*****************************************************
* ATTENTION : le mode surtif semble activé          *
* Penser à le signaler à votre CMI                  *
* (une configuration sur le serveur est nécessaire) *
*****************************************************
EOF
else
    cat <<EOF
Si vous activez un jour le mode furtif, il faudra penser à le signaler :
un réglage devra être modifié sur le serveur.
EOF
fi

