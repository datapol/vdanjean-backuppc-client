#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t broken-links:nil c:nil creator:nil
#+OPTIONS: d:(not "LOGBOOK") date:t e:t email:nil f:t inline:t num:t
#+OPTIONS: p:nil pri:nil prop:nil stat:t tags:t tasks:t tex:t
#+OPTIONS: timestamp:t title:t todo:t |:t
#+TITLE: Documentation dédiée à la sauvegarde des machines de datapol
#+DATE: <2017-07-03 lun.>
#+AUTHOR: Vincent Danjean
#+EMAIL: Vincent.Danjean@ens-lyon.org
#+LANGUAGE: fr
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 25.1.1 (Org mode 9.0.3)


#+TOC: headlines 3        insert TOC here, with three headline levels

* Mise en place de la sauvegarde de votre machine
** Avec Debian ou Ubuntu
    Passer root
#+begin_src sh
sudo su
#+end_src
    puis effectuer les étapes suivantes :

1. Ajout la nouvelle source de paquets dans =/etc/apt/sources.list= (ou dans
   un fichier dans le répertoire =/etc/apt/sources.list.d/=)
   #+begin_src sh
   cat > /etc/apt/sources.list.d/datapol.sources <<EOF
   Types: deb
   URIs: http://moais.imag.fr/membres/vincent.danjean/debian
   Suites: stable
   Components: main imag
   Signed-By: /usr/share/keyrings/vdanjean-archive-keyring.gpg
   EOF
#+end_src

2. Installer manuellement le paquet vdanjean-archive-keyring qui
   installe la clé du nouveau dépot :

   #+begin_src sh 
   cd /tmp
   wget http://people.debian.org/~vdanjean/debian/pool/main/v/vdanjean-archive-keyring/vdanjean-archive-keyring_2023.02.24_all.deb
   dpkg -i vdanjean-archive-keyring_2023.02.24_all.deb
   rm vdanjean-archive-keyring_2023.02.24_all.deb
#+end_src

   Note: si le fichier n'existe pas, regarder avec un navigateur
   les fichiers du répertoire
   (http://people.debian.org/~vdanjean/debian/pool/main/v/vdanjean-archive-keyring),
   et utiliser le paquet mis à jour. Et si vous prévenez, cette documentation
   pourra être mise à jour.

3. Mettre à jour la liste des paquets.
   #+begin_src sh 
   apt-get update
#+end_src

4. Installer le paquet =datapol-backuppc=
   #+begin_src sh 
   apt-get install datapol-backuppc
#+end_src
   Avec ce paquet installé, le programme backuppc de la machine lig-backuppc2
   pourra se connecter sur votre machine et lancer un rsync avec les
   droits root. Voir =/var/lib/datapol-backuppc/.ssh/authorized_keys= pour
   les détails.

5. Contacter les CMI (de préférence Vincent Danjean) pour reconfigurer
   backuppc et ajouter votre machine à la liste des machines
   sauvegardées. Prévoir de donner votre IP sur le réseau interne filaire
   (utiliser =ip a= et trouver l'IP commençant par =129.88...=) ainsi que
   la liste des répertoires que vous désirerez sauvegarder (vous pourrez
   exclure des choses, voire le point suivant)

6. Lire *Choix des fichiers sauvegardés* pour exclure si nécessaire des
   fichiers ou des dossiers de la sauvegarde

** Avec une autre distribution Linux

A priori, pas de support CMI pour mettre en place ce que font les
paquets de la solution précédente. Un utilisateur motivé peut
installer manuellement le contenu des paquets précédents. Si un rpm ou
autre paquet est créé, n'hésitez pas à le signaler ici pour que les
autres utilisateurs puissent en profiter.

** Avec MacOS X

1. Récupérer le script =MacOSX.install= sur le gitlab Inria dans le
   groupe datapol et l'exécuter en tant que root :

   #+begin_src sh 
   wget -N https://gitlab.inria.fr/datapol/vdanjean-backuppc-client/raw/master/MacOSX/MacOSX.install
   chmod +x MacOSX.install
   # Les gens sérieux regardent le contenu du script avant de l'exécuter...
   sudo bash ./MacOSX.install
   #+end_src

2. Contacter les CMI (de préférence Vincent Danjean) pour reconfigurer
   backuppc et ajouter votre machine à la liste des machines
   sauvegardées.

3. Lire *Choix des fichiers sauvegardés* pour exclure si nécessaire des
   fichiers ou des dossiers de la sauvegarde

Note : MacOS X vient avec =rsync= 2.6.9 (i.e. release de 2006 !). Certaines
fonctionnalités de =rsync= doivent donc être désactivées (sauvegarde des ACL,
des attributs étendus, etc.). Si vous installez manuellement une version
plus récente de =rsync=, signalez-le pour qu'on réactive ces fonctionnalités
dans les sauvegardes.

* Choix des fichiers sauvegardés

Lors de l'installation avec les CMI (avant dernière étape), vous avez
choisi un ou plusieurs répertoires à sauver. La sauvegarder est basée
sur rsync. Vous pouvez choisir précisément ce qui est sauvegardé en
utilisant des fichiers =.rsync-datapol-filter= dans vos
répertoires. =rsync= est invoqué avec l'option
=--filter=dir-merge /.rsync-datapol-filter=. Veuillez regarder la
[[http://linuxcommand.org/man_pages/rsync1.html][page de manuel de rsync]] (section
"FILTER RULES") pour plus d'info. Il est possible que les CMI mettent
en place un filtre supplémentaire par défaut (*.o, ...) Ce n'est pas
fait pour l'instant.

Par exemple, supposons que le répertoire =/home/person/foo/= doit être
sauvegardé et que le fichier =/home/person/foo/.rsync-datapol-filter=
contient:

#+begin_src sh 
- *~
- *.o  
- bar/
- /bar2/
- *NO_BACKUP*
#+end_src

La sauvegarde exclura le dossier =/home/person/foo/bar2=, tous les
dossiers (et sous-dossiers) =bar/= et tous les fichiers ou dossier
dont le nom contient
=NO_BACKUP=, finit par =~= ou par =.o=. Plus de détails sur
[[http://linuxcommand.org/man_pages/rsync1.html]] rubrique
"INCLUDE/EXCLUDE PATTERN RULES"

Note : l'ancien fichier de règle =.rsync-memo-filter= est encore
supporté pour l'instant mais il vaut mieux utiliser
=.rsync-datapol-filter= désormais. Vous pouvez juste renommer vos
anciens fichiers de règles si vous en avez.

* Sauvegarde de dossiers cryptés montés via fuse

Si vous voulez sauvegarder un dossier monté via fuse (coffre fort
crypté avec encfs par exemple), il est nécessaire d'autoriser l'accès
à ce dossier pour root.

Pour cela il faut d'abord autoriser les utilisateurs a monter des
dossiers visibles par d'autre, éditez le fichier =/etc/fuse.conf= et
ajoutez la ligne : =user_allow_other=.

Enfin passez l'option =-o allow_root= a fuse dans votre commande de montage.

* Allumage/extinction automatique du serveur ssh

BackupPC utilise le démon ssh afin d’effectuer des sauvegardes or il
est rarement nécessaire d'avoir un serveur ssh sur un portable. Bien
que ssh soit très sécurisé (à condition d'être correctement
configuré), il est toujours bon de désactiver les points d'entrée
inutiles dans nos ordinateurs. Voici une technique pour s'assurer que
le serveur ssh ne soit actif que quand l'ordinateur est connecté au
réseau Inria ou LIG et pour bloquer le port 22 via le firewall ufw.

Tout d'abord on désactive le serveur ssh par défaut. Si vous utilisez
systemd:

#+begin_src sh 
sudo systemctl disable --now ssh
#+end_src

sinon:

#+begin_src sh 
sudo update-rc.d -f ssh disable
#+end_src

Créez ensuite un fichier =eth.sh= dans =/usr/local/sbin= (par exemple)
contenant le code suivant:

#+begin_src sh 
#!/bin/bash
sleep 5 # Attendons que l'IP soit mise a jour
INRIA=$(ip -4 a ls | grep "inet 194.199.27")
LIG=$(ip -4 a ls | grep "inet 129.88.54")
if [ -z "$INRIA" -a -z "$LIG" ]
then
    logger "Not connected to Inria or LIG network"
    ufw deny ssh
    systemctl stop ssh
else
    logger "Connected to Inria or LIG network: $INRIA$LIG"
    ufw allow ssh
    systemctl start ssh
fi
#+end_src

Notes:

    Ce code utilise systemctl, si vous êtes toujours sur initd,
    changez les commandes en service ssh start et service ssh stop.

    L'utilisation de ufw est une sécurité supplémentaire, vous pouvez
    modifier/supprimer les lignes si vous préférez utiliser un autre
    ou aucun pare-feu.

Enfin assurez vous que ce script soit exécuté à chaque connexion /
déconnexion réseau

#+begin_src sh 
sudo ln -s /usr/loca/sbin/eth.sh /etc/network/if-up.d/inria
sudo ln -s /usr/loca/sbin/eth.sh /etc/network/if-down.d/inria
#+end_src

Après avoir fait ces manipulations vous pouvez vérifier que les
modifications ont bien lieu lors des connections/déconnections :

#+begin_src sh 
sudo systemctl status ssh; ufw status verbose
#+end_src

* Consultation/restauration des fichiers sauvegardés

Cela se fait depuis
l'[[https://lig-backuppc2.imag.fr/backuppc/][interface web]].
