
include config/$(NAME)

LOGINNAME ?= _$(NAME)
USERNAME=$(LOGINNAME)-backuppc

PKG_GEN_FILES+=dirs install postinst postrm preinst
GEN_FILES+= $(addprefix debian/$(PACKAGE).,$(PKG_GEN_FILES))

define copy-subst-from-common
debian/$$(PACKAGE).$1: debian/debhelper.in/$1.in
	cat $$< \
	| if [ -f debian/debhelper.in/$$(PACKAGE).$1.in ]; then \
	    sed -e '/^#TARGET_HELPER#/r debian/debhelper.in/$$(PACKAGE).$1.in' \
		-e '/^#TARGET_HELPER#/d' ;\
	else \
	    sed -e 's/^#TARGET_HELPER#//' ;\
	fi \
	| sed -e "s|#PACKAGE#|$$(PACKAGE)|g" \
	| sed -e "s|#NAME#|$$(NAME)|g" \
	| sed -e "s|#USERNAME#|$$(USERNAME)|g" \
	> $$@
	$2

debian/$$(PACKAGE).$1: $$(wildcard debian/debhelper.in/$$(PACKAGE).$1.in)
endef
