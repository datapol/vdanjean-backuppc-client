
.PHONY: all all-
all: all-$(NAME)

all-:
	@echo "NAME must be set"
	exit 1

VARS=NAME AUTHORIZED_IPS LOGINNAME

all-%: D=$*
all-%: NAME=$*
all-%: config/%
	$(eval include config/$*)
	$(eval LOGINNAME ?= _$*)
	mkdir -p build/$D
	cp -a files/. build/$D/.
	find build/$D/ -depth -name "*NAME*" -printf "%p\n" | \
		while read n ; do \
			mv -v "$$n" "$$(echo $$n | sed -e 's/\(.*\)NAME/\1$*/')" ; \
		done
	find build/$D/ -depth -name "*.in" -printf "%p\n" | \
		while read n ; do \
			sed < "$$n" > "$$(echo $$n | sed -e 's/\.in$$//')" \
				$(foreach v,$(VARS), -e 's/@$v@/$($v)/g' ) \
				-e 's|@SSH_PUBKEY@|$(SSH_PUBKEY)|g' ;\
			test ! -x "$$n" || chmod +x "$$(echo $$n | sed -e 's/\.in$$//')" ;\
			$(RM) -v "$$n" ;\
		done
	$(foreach s,$(SSH_EXTRA_CONFIG),\
		echo "*** EXTRA SSH CONFIG $s" && \
		sed < files/skell/NAME-backuppc/.ssh/authorized_keys.in >> build/$D/skell/$*-backuppc/.ssh/authorized_keys \
			-e 's/@AUTHORIZED_IPS@/$(AUTHORIZED_IPS_$s)/g' \
			-e 's|@SSH_PUBKEY@|$(SSH_PUBKEY_$s)|g' && ) true


clean::
	$(RM) -rv build
	$(MAKE) -C MacOSX $@

clean-%: D=$*
clean-%:
	$(RM) -rv build/$D
