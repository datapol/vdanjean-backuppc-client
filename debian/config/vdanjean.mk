
PACKAGE=vdanjean-backuppc
include debian/config/common.mk

$(foreach f,$(filter-out install,$(PKG_GEN_FILES)), \
  $(eval $(call copy-subst-from-common,$f)))

$(eval $(call copy-subst-from-common,install, \
	$(foreach d,/var/local /var/www, \
		echo 'debian/data/vdanjean-backuppc $d' >> $$@ ;)))

