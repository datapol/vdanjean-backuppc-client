# les chemins commençant par / s'applique au répertoire courant
# les chemins relatifs (ne commençant pas par /) sont recherchés
# dans tous les sous-réperoires.

- *.o    # tous les .o dans tous les sous-répertoires
- /*.o   # seulement ceux du répertoire courant

# La syntaxe d'un fichier de règles rsync permet de faire plein de choses
# La lecture de la documentation complète est nécessaire pour apprécier
# toutes les fonctionnalités.

# Exemple (extrait de la documentation) pour n'inclure qu'un sous-répertoire
# particulier. Il faut bien se souvenir qu'rsync utilise la première règle
# qui matche le chemin en cours de test.
# Sans les deux premières lignes, rsync n'irai pas voir dans les
# sous-répertoires (donc ne trouverait jamais /some/path/this-file-is-found)

+ /some/
+ /some/path/
+ /some/path/this-file-is-found
+ /file-also-included
- *

